import { useState, useEffect } from 'react';
import axios from 'axios'

function useAsyncHook(text: string) {
    const [compressedString, setCompressedString] = useState('');
    const [error, setError] = useState(Boolean);
    useEffect(() => {
        async function fetchCompressedString() {
            try {
                const response = await axios.get(`http://localhost:8080/group/${text}`)
                setCompressedString(response.data);
                setError(false);
            } catch (err) {
                console.log(err)
                setError(true)
            }
        }
        if (text !== "") {
            fetchCompressedString();
        }
    }, [text]);
    return [compressedString, error];
}

export default useAsyncHook
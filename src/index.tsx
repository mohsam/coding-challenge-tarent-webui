import React from 'react';
import ReactDOM from 'react-dom';
import CompressApp from './components/CompressApp';
import './styles/styles.scss';

ReactDOM.render(<CompressApp />, document.getElementById('root'));

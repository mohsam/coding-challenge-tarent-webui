import React from 'react';
import { shallow } from 'enzyme';
import { render, getByTestId, fireEvent } from '@testing-library/react';
import CompressForm from '../../components/CompressForm';

test('should render CompressForm correctly', () => {
    const wrapper = shallow(<CompressForm />);
    expect(wrapper).toMatchSnapshot();
});

test('App loads with initial input value of empty', () => {
    const { container } = render(<CompressForm />);
    const inputValue = getByTestId(container, 'inputValue');
    expect(inputValue.textContent).toBe('');
});

test('should set input field value', () => {
    const { container } = render(<CompressForm />);
    const inputValue = getByTestId(container, 'inputValue') as HTMLInputElement;
    const submitButton = getByTestId(container, 'submitButton');
    const newValue = 'hello';

    fireEvent.change(inputValue, {
        target: { value: newValue },
    });
    fireEvent.click(submitButton);
    expect(inputValue.value).toBe(newValue);
});

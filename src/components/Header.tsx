import React from 'react';

const Header = () => {
    return (
        <div className="header">
            <h1 className="header__title">Sort and Compress Text!</h1>
        </div>
    );
};

export default Header;

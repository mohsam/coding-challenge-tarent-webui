import React from 'react';
import Header from './Header';
import CompressForm from './CompressForm';

const CompressApp = () => {
    return (
        <div className="container">
            <Header />
            <CompressForm />
        </div>
    );
};

export default CompressApp;

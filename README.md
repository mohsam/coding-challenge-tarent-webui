## Overview

This is a single page application, implemented using react and typescript as a coding challenge requirement for Tarent.
You can enter a string then click compress now! It will be sorted and compressed if more than 2 of the same letter exist.
Example: string entered: abzuaaissna result: a4binssuz

## Requirements

-   Node

## Dependencies

-   this app depends on the spring boot [webservice](https://bitbucket.org/moh_sam94/coding-challenge-tarent-webservice)

## Install

`npm install`

### Start Development Server

`npm run dev`

### Start Production Server

`npm run start`

### Test

`npm run test`
`npm run test-watch`

### Build

`npm run build`

Open [http://localhost:3000](http://localhost:3000) to view development server in the browser.
Open [http://localhost:5000](http://localhost:5000) to view production server in the browser.
